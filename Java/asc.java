// class Asc
// polymorphism, encapsulation and inheritance
class Asc {
    public static void main (String a[]) {
        int i;
        String hex = "0";
        char c ='0';
        for (i =0;i < 256;i++) {
            c = (char)i;
            hex = Integer.toHexString(i);
            System.out.print(i+" "+c+" "+hex+" ");
            if(i % 5 == 0)System.out.println();
        }
    }
}